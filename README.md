Upload backups to Google Drive
======

First you need install gDrive in your server, see: https://gitlab.com/jhosman55/gdrive and configure your Google Drive Account

## Installation

```
wget https://gitlab.com/jhosman55/backupsgdrive/raw/master/telegram_bot
mv telegram_bot /bin/
vi /bin/telegram_bot
```

Remove the lines of help

And configure your telegram bot, Enjoy! =)
